certifi==2020.6.20
cycler==0.10.0
kiwisolver==1.2.0
matplotlib==3.3.1
numpy==1.19.1
Pillow==7.2.0
pyparsing==2.4.7
python-dateutil==2.8.1
scipy==1.5.2
six==1.15.0

requests~=2.24.0
setuptools~=50.1.0
quantiphy~=2.12.0