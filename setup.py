from setuptools import setup, find_packages

with open('requirements.txt') as fp:
    install_requires = fp.read()

setup(
    name='csttoolkit',
    version='1.0.0',
    packages=find_packages(),
    install_requires=install_requires,
    include_package_data=True,
    url='',
    license='',
    author='Steven Stroka',
    author_email='',
    description=''
)
