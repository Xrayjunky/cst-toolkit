import argparse

from csttoolkit.result import Normal, CSTPlotter

parser = argparse.ArgumentParser()
parser.add_argument('--res', nargs='+')

args = parser.parse_args()

data = CSTPlotter(args.res)
data.normal(Normal.DIRECTION_Z, -0.33)
data.show()
