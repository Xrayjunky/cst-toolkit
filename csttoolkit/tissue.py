from pathlib import Path

import requests
from quantiphy import Quantity


def load_cst_template():
    """
    Load the template file provided by CST
    :return: dictionary
    """

    dict_tissue_cst = {}

    with open('csttoolkit/data/template/tissues_template.txt') as file:
        lines = file.readlines()[23:]
        for line in lines:
            line = line.split("\t")
            line = [x.strip() for x in line]

            dict_tissue_cst[line[0]] = {'number': int(line[1]),
                                        'epsilon': float(line[2]),
                                        'mu': int(line[3]),
                                        'kappa': float(line[4]),
                                        'rho': int(line[5]),
                                        'k': line[6],
                                        'heat_cap': line[7],
                                        'blood_flow': line[8],
                                        'metabol': line[9],
                                        'r': int(line[10]),
                                        'g': int(line[11]),
                                        'b': int(line[12])}

    return dict_tissue_cst


def load_tissues_from_gabriel(frequency):
    """
    Load tissue properties from the internet resource for the calculation of the
    dielectric properties of body tissues in the frequency rang 10 Hz - 100 GHz
    :param frequency: i.e. 5000000 or 5e6
    :return: dictionary with dielectric properties of body tissues for the given frequency
    """
    Path("output").mkdir(parents=True, exist_ok=True)

    URL = 'http://niremf.ifac.cnr.it/tissprop/htmlclie/uniquery.php'
    FILE = 'output/tissue_gabriel.txt'
    PAYLOAD = {'func': 'atsffun',
               'freq': frequency,
               'tiss': '',
               'outform': 'downtxt',
               'tisname': 'on',
               'frequen': 'on',
               'conduct': 'on',
               'permitt': 'on',
               'losstan': 'on',
               'wavelen': 'on',
               'pendept': 'on',
               'freq1': frequency,
               'tissue2': 'Air',
               'frqbeg': 10,
               'frqend': 100e9,
               'linstep': 100,
               'mode': 'log',
               'logstep': 5,
               'tissue3': 'Air',
               'freq3': 1000000
               }

    response = requests.get(URL, PAYLOAD)

    open(FILE, 'wb').write(response.content)

    dict_tissue_gabriel = {}

    with open('output/tissue_gabriel.txt') as file:
        lines = file.readlines()[6:]
        for line in lines:
            line = line.split()

            dict_tissue_gabriel[line[0]] = {'frequency': float(line[1]),
                                            'conductivivty': float(line[2]),
                                            'relative_permittivity': float("{:0.2f}".format(float(line[3]))),
                                            'loss_tangent': float(line[4]),
                                            'wavelength': float(line[5]),
                                            'penetration_depth': line[6]}

    return dict_tissue_gabriel


def generate_tissue_cst_file(frequency):
    """
    Generate a CST compatible tissue file
    :param frequency: i.e. 5000000 or 5e6
    """
    dict_tissue_cst = load_cst_template()
    dict_tissue_gabriel = load_tissues_from_gabriel(frequency=frequency)

    dict_tissue_cst['Adrenals']['epsilon'] = dict_tissue_gabriel['Gland']['relative_permittivity']
    dict_tissue_cst['Adrenals']['kappa'] = dict_tissue_gabriel['Gland']['conductivivty']

    dict_tissue_cst['Blood']['epsilon'] = dict_tissue_gabriel['Blood']['relative_permittivity']
    dict_tissue_cst['Blood']['kappa'] = dict_tissue_gabriel['Blood']['conductivivty']

    dict_tissue_cst['Bone cortical']['epsilon'] = dict_tissue_gabriel['BoneCortical'][
        'relative_permittivity']
    dict_tissue_cst['Bone cortical']['kappa'] = dict_tissue_gabriel['BoneCortical']['conductivivty']

    dict_tissue_cst['Bone cortical']['epsilon'] = dict_tissue_gabriel['BoneCortical'][
        'relative_permittivity']
    dict_tissue_cst['Bone cortical']['kappa'] = dict_tissue_gabriel['BoneCortical']['conductivivty']

    dict_tissue_cst['Bone medullary cavity']['epsilon'] = dict_tissue_gabriel['BoneCancellous'][
        'relative_permittivity']
    dict_tissue_cst['Bone medullary cavity']['kappa'] = dict_tissue_gabriel['BoneCancellous'][
        'conductivivty']

    dict_tissue_cst['Bone spongiosa']['epsilon'] = dict_tissue_gabriel['BoneCancellous'][
        'relative_permittivity']
    dict_tissue_cst['Bone spongiosa']['kappa'] = dict_tissue_gabriel['BoneCancellous']['conductivivty']

    dict_tissue_cst['Brain']['epsilon'] = ((dict_tissue_gabriel['BrainGreyMatter']['relative_permittivity'] -
                                            dict_tissue_gabriel['BrainWhiteMatter']['relative_permittivity']) / 2) + \
                                          dict_tissue_gabriel['BrainWhiteMatter']['relative_permittivity']

    dict_tissue_cst['Brain']['kappa'] = ((dict_tissue_gabriel['BrainGreyMatter'][
                                              'conductivivty'] -
                                          dict_tissue_gabriel['BrainWhiteMatter'][
                                              'conductivivty']) / 2) + \
                                        dict_tissue_gabriel['BrainWhiteMatter']['conductivivty']

    dict_tissue_cst['Breast glandular tissue']['epsilon'] = dict_tissue_gabriel['Gland'][
        'relative_permittivity']
    dict_tissue_cst['Breast glandular tissue']['kappa'] = dict_tissue_gabriel['Gland']['conductivivty']

    dict_tissue_cst['Bronchi']['epsilon'] = dict_tissue_gabriel['Trachea']['relative_permittivity']
    dict_tissue_cst['Bronchi']['kappa'] = dict_tissue_gabriel['Trachea']['conductivivty']

    dict_tissue_cst['Cartilage']['epsilon'] = dict_tissue_gabriel['Cartilage']['relative_permittivity']
    dict_tissue_cst['Cartilage']['kappa'] = dict_tissue_gabriel['Cartilage']['conductivivty']

    dict_tissue_cst['Colon contents']['epsilon'] = dict_tissue_gabriel['Colon']['relative_permittivity']
    dict_tissue_cst['Colon contents']['kappa'] = dict_tissue_gabriel['Colon']['conductivivty']

    dict_tissue_cst['Colon wall']['epsilon'] = dict_tissue_gabriel['Colon']['relative_permittivity']
    dict_tissue_cst['Colon wall']['kappa'] = dict_tissue_gabriel['Colon']['conductivivty']

    dict_tissue_cst['Eye bulb']['epsilon'] = dict_tissue_gabriel['BodyFluid']['relative_permittivity']
    dict_tissue_cst['Eye bulb']['kappa'] = dict_tissue_gabriel['BodyFluid']['conductivivty']

    dict_tissue_cst['Eye lens']['epsilon'] = dict_tissue_gabriel['Lens']['relative_permittivity']
    dict_tissue_cst['Eye lens']['kappa'] = dict_tissue_gabriel['Lens']['conductivivty']

    dict_tissue_cst['Fat']['epsilon'] = dict_tissue_gabriel['Fat']['relative_permittivity']
    dict_tissue_cst['Fat']['kappa'] = dict_tissue_gabriel['Fat']['conductivivty']

    dict_tissue_cst['Gall bladder contents']['epsilon'] = dict_tissue_gabriel['GallBladderBile'][
        'relative_permittivity']
    dict_tissue_cst['Gall bladder contents']['kappa'] = dict_tissue_gabriel['GallBladderBile'][
        'conductivivty']

    dict_tissue_cst['Gall bladder']['epsilon'] = dict_tissue_gabriel['GallBladder'][
        'relative_permittivity']
    dict_tissue_cst['Gall bladder']['kappa'] = dict_tissue_gabriel['GallBladder']['conductivivty']

    dict_tissue_cst['Heart']['epsilon'] = dict_tissue_gabriel['Heart']['relative_permittivity']
    dict_tissue_cst['Heart']['kappa'] = dict_tissue_gabriel['Heart']['conductivivty']

    dict_tissue_cst['Kidney cortex']['epsilon'] = dict_tissue_gabriel['Kidney']['relative_permittivity']
    dict_tissue_cst['Kidney cortex']['kappa'] = dict_tissue_gabriel['Kidney']['conductivivty']

    dict_tissue_cst['Kidney medulla']['epsilon'] = dict_tissue_gabriel['Kidney']['relative_permittivity']
    dict_tissue_cst['Kidney medulla']['kappa'] = dict_tissue_gabriel['Kidney']['conductivivty']

    dict_tissue_cst['Kidney pelvis']['epsilon'] = dict_tissue_gabriel['Kidney']['relative_permittivity']
    dict_tissue_cst['Kidney pelvis']['kappa'] = dict_tissue_gabriel['Kidney']['conductivivty']

    dict_tissue_cst['Liver']['epsilon'] = dict_tissue_gabriel['Liver']['relative_permittivity']
    dict_tissue_cst['Liver']['kappa'] = dict_tissue_gabriel['Liver']['conductivivty']

    dict_tissue_cst['Lung']['epsilon'] = ((dict_tissue_gabriel['LungDeflated'][
                                               'relative_permittivity'] -
                                           dict_tissue_gabriel['LungInflated'][
                                               'relative_permittivity']) / 2) + \
                                         dict_tissue_gabriel['LungInflated'][
                                             'relative_permittivity']

    dict_tissue_cst['Lung']['kappa'] = ((dict_tissue_gabriel['LungDeflated'][
                                             'conductivivty'] -
                                         dict_tissue_gabriel['LungInflated'][
                                             'conductivivty']) / 2) + \
                                       dict_tissue_gabriel['LungInflated']['conductivivty']

    dict_tissue_cst['Lymphatic nodes']['epsilon'] = dict_tissue_gabriel['Lymph']['relative_permittivity']
    dict_tissue_cst['Lymphatic nodes']['kappa'] = dict_tissue_gabriel['Lymph']['conductivivty']

    dict_tissue_cst['Mucosa']['epsilon'] = dict_tissue_gabriel['MucousMembrane']['relative_permittivity']
    dict_tissue_cst['Mucosa']['kappa'] = dict_tissue_gabriel['MucousMembrane']['conductivivty']

    dict_tissue_cst['Muscle']['epsilon'] = dict_tissue_gabriel['Muscle']['relative_permittivity']
    dict_tissue_cst['Muscle']['kappa'] = dict_tissue_gabriel['Muscle']['conductivivty']

    dict_tissue_cst['Oesophagus']['epsilon'] = dict_tissue_gabriel['Oesophagus']['relative_permittivity']
    dict_tissue_cst['Oesophagus']['kappa'] = dict_tissue_gabriel['Oesophagus']['conductivivty']

    dict_tissue_cst['Ovaries']['epsilon'] = dict_tissue_gabriel['Ovary']['relative_permittivity']
    dict_tissue_cst['Ovaries']['kappa'] = dict_tissue_gabriel['Ovary']['conductivivty']

    dict_tissue_cst['Pancreas']['epsilon'] = dict_tissue_gabriel['Pancreas']['relative_permittivity']
    dict_tissue_cst['Pancreas']['kappa'] = dict_tissue_gabriel['Pancreas']['conductivivty']

    dict_tissue_cst['Pituitary gland']['epsilon'] = dict_tissue_gabriel['Pancreas'][
        'relative_permittivity']
    dict_tissue_cst['Pituitary gland']['kappa'] = dict_tissue_gabriel['Pancreas']['conductivivty']

    dict_tissue_cst['Prostate']['epsilon'] = dict_tissue_gabriel['Prostate']['relative_permittivity']
    dict_tissue_cst['Prostate']['kappa'] = dict_tissue_gabriel['Prostate']['conductivivty']

    dict_tissue_cst['Penis']['epsilon'] = dict_tissue_gabriel['SkinDry']['relative_permittivity']
    dict_tissue_cst['Penis']['kappa'] = dict_tissue_gabriel['SkinDry']['conductivivty']

    dict_tissue_cst['Muscle fat average']['epsilon'] = ((dict_tissue_gabriel['Muscle'][
                                                             'relative_permittivity'] -
                                                         dict_tissue_gabriel['Fat'][
                                                             'relative_permittivity']) / 2) + \
                                                       dict_tissue_gabriel['Fat'][
                                                           'relative_permittivity']

    dict_tissue_cst['Muscle fat average']['kappa'] = ((dict_tissue_gabriel['Muscle'][
                                                           'conductivivty'] -
                                                       dict_tissue_gabriel['Fat'][
                                                           'conductivivty']) / 2) + \
                                                     dict_tissue_gabriel['Fat']['conductivivty']

    dict_tissue_cst['Salivary glands']['epsilon'] = dict_tissue_gabriel['Pancreas'][
        'relative_permittivity']
    dict_tissue_cst['Salivary glands']['kappa'] = dict_tissue_gabriel['Pancreas']['conductivivty']

    dict_tissue_cst['Skin']['epsilon'] = ((dict_tissue_gabriel['SkinWet'][
                                               'relative_permittivity'] -
                                           dict_tissue_gabriel['SkinDry'][
                                               'relative_permittivity']) / 2) + \
                                         dict_tissue_gabriel['SkinDry'][
                                             'relative_permittivity']

    dict_tissue_cst['Skin']['kappa'] = ((dict_tissue_gabriel['SkinDry'][
                                             'conductivivty'] -
                                         dict_tissue_gabriel['SkinWet'][
                                             'conductivivty']) / 2) + \
                                       dict_tissue_gabriel['SkinWet']['conductivivty']

    dict_tissue_cst['Small intestine contents']['epsilon'] = dict_tissue_gabriel['SmallIntestine'][
        'relative_permittivity']
    dict_tissue_cst['Small intestine contents']['kappa'] = dict_tissue_gabriel['SmallIntestine'][
        'conductivivty']

    dict_tissue_cst['Small intestine wall']['epsilon'] = dict_tissue_gabriel['SmallIntestine'][
        'relative_permittivity']
    dict_tissue_cst['Small intestine wall']['kappa'] = dict_tissue_gabriel['SmallIntestine'][
        'conductivivty']

    dict_tissue_cst['Nerve']['epsilon'] = dict_tissue_gabriel['Nerve']['relative_permittivity']
    dict_tissue_cst['Nerve']['kappa'] = dict_tissue_gabriel['Nerve']['conductivivty']

    dict_tissue_cst['Spleen']['epsilon'] = dict_tissue_gabriel['Spleen']['relative_permittivity']
    dict_tissue_cst['Spleen']['kappa'] = dict_tissue_gabriel['Spleen']['conductivivty']

    dict_tissue_cst['Stomach contents']['epsilon'] = dict_tissue_gabriel['Stomach'][
        'relative_permittivity']
    dict_tissue_cst['Stomach contents']['kappa'] = dict_tissue_gabriel['Stomach']['conductivivty']

    dict_tissue_cst['Stomach wall']['epsilon'] = dict_tissue_gabriel['Stomach']['relative_permittivity']
    dict_tissue_cst['Stomach wall']['kappa'] = dict_tissue_gabriel['Stomach']['conductivivty']

    dict_tissue_cst['Teeth']['epsilon'] = dict_tissue_gabriel['Tooth']['relative_permittivity']
    dict_tissue_cst['Teeth']['kappa'] = dict_tissue_gabriel['Tooth']['conductivivty']

    dict_tissue_cst['Testes']['epsilon'] = dict_tissue_gabriel['Testis']['relative_permittivity']
    dict_tissue_cst['Testes']['kappa'] = dict_tissue_gabriel['Testis']['conductivivty']

    dict_tissue_cst['Small intestine']['epsilon'] = dict_tissue_gabriel['SmallIntestine'][
        'relative_permittivity']
    dict_tissue_cst['Small intestine']['kappa'] = dict_tissue_gabriel['SmallIntestine']['conductivivty']

    dict_tissue_cst['Thymus']['epsilon'] = dict_tissue_gabriel['Thymus']['relative_permittivity']
    dict_tissue_cst['Thymus']['kappa'] = dict_tissue_gabriel['Thymus']['conductivivty']

    dict_tissue_cst['Thyroid']['epsilon'] = dict_tissue_gabriel['Thyroid']['relative_permittivity']
    dict_tissue_cst['Thyroid']['kappa'] = dict_tissue_gabriel['Thyroid']['conductivivty']

    dict_tissue_cst['Tongue']['epsilon'] = dict_tissue_gabriel['Tongue']['relative_permittivity']
    dict_tissue_cst['Tongue']['kappa'] = dict_tissue_gabriel['Tongue']['conductivivty']

    dict_tissue_cst['Tonsils']['epsilon'] = dict_tissue_gabriel['Thyroid']['relative_permittivity']
    dict_tissue_cst['Tonsils']['kappa'] = dict_tissue_gabriel['Thyroid']['conductivivty']

    dict_tissue_cst['Trachea']['epsilon'] = dict_tissue_gabriel['Trachea']['relative_permittivity']
    dict_tissue_cst['Trachea']['kappa'] = dict_tissue_gabriel['Trachea']['conductivivty']

    dict_tissue_cst['Ureter']['epsilon'] = dict_tissue_gabriel['BloodVessel']['relative_permittivity']
    dict_tissue_cst['Ureter']['kappa'] = dict_tissue_gabriel['BloodVessel']['conductivivty']

    dict_tissue_cst['Urinary bladder contents']['epsilon'] = dict_tissue_gabriel['BodyFluid'][
        'relative_permittivity']
    dict_tissue_cst['Urinary bladder contents']['kappa'] = dict_tissue_gabriel['BodyFluid'][
        'conductivivty']

    dict_tissue_cst['Urinary bladder wall']['epsilon'] = dict_tissue_gabriel['Bladder'][
        'relative_permittivity']
    dict_tissue_cst['Urinary bladder wall']['kappa'] = dict_tissue_gabriel['Bladder']['conductivivty']

    dict_tissue_cst['Uterus Cervix']['epsilon'] = dict_tissue_gabriel['Uterus']['relative_permittivity']
    dict_tissue_cst['Uterus Cervix']['kappa'] = dict_tissue_gabriel['Uterus']['conductivivty']

    dict_tissue_cst['Air inside body']['epsilon'] = dict_tissue_gabriel['Air']['relative_permittivity']
    dict_tissue_cst['Air inside body']['kappa'] = dict_tissue_gabriel['Air']['conductivivty']

    dict_tissue_cst['Mucosa anterior nasal']['epsilon'] = dict_tissue_gabriel['MucousMembrane'][
        'relative_permittivity']
    dict_tissue_cst['Mucosa anterior nasal']['kappa'] = dict_tissue_gabriel['MucousMembrane'][
        'conductivivty']

    dict_tissue_cst['Mucosa posterior nasal']['epsilon'] = dict_tissue_gabriel['MucousMembrane'][
        'relative_permittivity']
    dict_tissue_cst['Mucosa posterior nasal']['kappa'] = dict_tissue_gabriel['MucousMembrane'][
        'conductivivty']

    dict_tissue_cst['Bone average']['epsilon'] = dict_tissue_gabriel['BoneCortical'][
        'relative_permittivity']
    dict_tissue_cst['Bone average']['kappa'] = dict_tissue_gabriel['BoneCortical']['conductivivty']

    dict_tissue_cst['Kidney average']['epsilon'] = dict_tissue_gabriel['Kidney']['relative_permittivity']
    dict_tissue_cst['Kidney average']['kappa'] = dict_tissue_gabriel['Kidney']['conductivivty']

    dict_tissue_cst['BloodV']['epsilon'] = dict_tissue_gabriel['Blood']['relative_permittivity']
    dict_tissue_cst['BloodV']['kappa'] = dict_tissue_gabriel['Blood']['conductivivty']

    dict_tissue_cst['Cerebrospinal fluid']['epsilon'] = dict_tissue_gabriel['CerebroSpinalFluid'][
        'relative_permittivity']
    dict_tissue_cst['Cerebrospinal fluid']['kappa'] = dict_tissue_gabriel['CerebroSpinalFluid'][
        'conductivivty']

    dict_tissue_cst['Ventricles']['epsilon'] = dict_tissue_gabriel['CerebroSpinalFluid'][
        'relative_permittivity']
    dict_tissue_cst['Ventricles']['kappa'] = dict_tissue_gabriel['CerebroSpinalFluid']['conductivivty']

    dict_tissue_cst['Brain grey matter']['epsilon'] = dict_tissue_gabriel['BrainGreyMatter'][
        'relative_permittivity']
    dict_tissue_cst['Brain white matter']['kappa'] = dict_tissue_gabriel['BrainWhiteMatter'][
        'conductivivty']

    dict_tissue_cst['Cerebellum']['epsilon'] = dict_tissue_gabriel['Cerebellum']['relative_permittivity']
    dict_tissue_cst['Cerebellum']['kappa'] = dict_tissue_gabriel['Cerebellum']['conductivivty']

    dict_tissue_cst['Bone ossicles']['epsilon'] = dict_tissue_gabriel['BoneCortical'][
        'relative_permittivity']
    dict_tissue_cst['Bone ossicles']['kappa'] = dict_tissue_gabriel['BoneCortical']['conductivivty']

    dict_tissue_cst['BloodA']['epsilon'] = dict_tissue_gabriel['Blood']['relative_permittivity']
    dict_tissue_cst['BloodA']['kappa'] = dict_tissue_gabriel['Blood']['conductivivty']

    dict_tissue_cst['Placenta']['epsilon'] = dict_tissue_gabriel['Blood']['relative_permittivity']
    dict_tissue_cst['Placenta']['kappa'] = dict_tissue_gabriel['Blood']['conductivivty']

    # TODO: Determine properties for a fetus
    # TODO: Change accuracy of the tissue properties by gabriel

    template_file = open('csttoolkit/data/template/tissues_template.txt', 'r')
    lines = template_file.readlines()
    template_file.close()

    Path("output").mkdir(parents=True, exist_ok=True)

    frequency_information = Quantity(frequency, 'Hz')
    lines[2] = "// Frequency: %s \n" % frequency_information
    frequency_information = frequency_information.as_tuple()[0]
    print(frequency_information)

    tissue_file = open("output/tissue.txt", 'w')

    for i in range(23):
        tissue_file.write(lines[i])

    for property_ in dict_tissue_cst:
        tissue_file.write(property_.ljust(24) + "\t" + str(dict_tissue_cst[property_]['number']) + "\t" +
                          str("{:0.2f}".format(dict_tissue_cst[property_]['epsilon'])) + "\t" + str(
            dict_tissue_cst[property_]['mu'])
                          + "\t" + str("{:.3f}".format(dict_tissue_cst[property_]['kappa'])) + "\t" + str(
            dict_tissue_cst[property_]['rho'])
                          + "\t" + str(dict_tissue_cst[property_]['k']) + "\t" + str(
            dict_tissue_cst[property_]['heat_cap'])
                          + "\t" + str(dict_tissue_cst[property_]['blood_flow']) + "\t" + str(
            dict_tissue_cst[property_]['metabol'])
                          + "\t" + str(dict_tissue_cst[property_]['r']) + "\t" + str(
            dict_tissue_cst[property_]['g'])
                          + "\t" + str(dict_tissue_cst[property_]['b']) + "\n")

    tissue_file.close()
