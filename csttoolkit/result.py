from scipy.interpolate import griddata
from matplotlib.colors import LogNorm
from enum import Enum

import numpy as np
import matplotlib.pyplot as plt
import re

FIELD_TYPE = ''

PLOT_X_LABEL = 'x/mm'
PLOT_Y_LABEL = 'y/mm'


class Normal(Enum):
    """
    Enum class to define each normal direction
    """
    DIRECTION_X = 0
    DIRECTION_Y = 1
    DIRECTION_Z = 2


class CSTPlotter:
    """
    Class to plot the field results computed by CST. A handing over of a list
    of generated files allows a superposition of the fields.

    IMPORTANT: ONLY TEXT FILES ARE COMPATIBLE


    Attributes
    ----------
    name: str | list
        filepath(s) of the result(s)


    Methods
    -------
    normal(direction=Normal.DIRECTION_X, position=0):
        Set the sectional plane
    absolute_field():
        Compute the absolute field based on CST generated files
    show():
        Show the field result

    """

    def __init__(self, name=None):

        self.vec1 = None
        self.vec2 = None
        self.field = None
        self.type = None
        self.list_abs = []

        if not isinstance(name, list):
            self.get_field_type(name)
            data = np.loadtxt(name, skiprows=2)
            self.x = data[:, 0]
            self.y = data[:, 1]
            self.z = data[:, 2]
            self.fx = data[:, 3]
            self.ifx = data[:, 4]
            self.fy = data[:, 5]
            self.ify = data[:, 6]
            self.fz = data[:, 7]
            self.ifz = data[:, 8]


        else:

            self.get_field_type(name[0])
            data_1 = np.loadtxt(name[0], skiprows=2)

            self.x = data_1[:, 0]
            self.y = data_1[:, 1]
            self.z = data_1[:, 2]
            self.fx = data_1[:, 3]
            self.ifx = data_1[:, 4]
            self.fy = data_1[:, 5]
            self.ify = data_1[:, 6]
            self.fz = data_1[:, 7]
            self.ifz = data_1[:, 8]

            for i in range(1, len(name)):
                self.get_field_type(name[i])
                data_2 = np.loadtxt(name[i], skiprows=2)

                self.fx += data_2[:, 3]
                self.ifx += data_2[:, 4]
                self.fy += data_2[:, 5]
                self.ify += data_2[:, 6]
                self.fz += data_2[:, 7]
                self.ifz += data_2[:, 8]

        self.fabs = self.absolute_field()

    def get_field_type(self, name):

        global FIELD_TYPE

        REGEX_E_FIELD = 'Ex|Ey|Ez'
        REGEX_H_FIELD = 'Hx|Hy|Hz'
        REGEX_CURRENT_DENSITY = 'Jx|Jy|Jz'

        with open(name) as file:
            first_line = file.readline()

            if re.search(REGEX_E_FIELD, first_line):
                FIELD_TYPE ='E-Feld / V/m'
            elif re.search(REGEX_H_FIELD, first_line):
                FIELD_TYPE = "H-Feld / A/m"
            # elif re.search(REGEX_CURRENT_DENSITY, first_line):
            #   FIELD_TYPE = "Current Density / A/m^2"

    def rotate_field(self):

        # r_x = np.sqrt(self.fx ** 2 + self.ifx ** 2)
        # r_y = np.sqrt(self.fy ** 2 + self.ify ** 2)
        # r_z = np.sqrt(self.fz ** 2 + self.ifz ** 2)

        # c_x = [complex(self.fx[i], self.ifx[i]) for i in range(len(self.fx))]
        # c_y = [complex(self.fy[i], self.ify[i]) for i in range(len(self.fy))]
        # c_z = [complex(self.fz[i], self.ifz[i]) for i in range(len(self.fz))]

        # phi_x = [cmath.phase(i) -10 for i in c_x]
        # phi_y = [cmath.phase(i) + 8 for i in c_y]
        # phi_z = [cmath.phase(i) for i in c_z]

        # self.fx = r_x * np.cos(np.sqrt(2)/2)
        # self.ifx = r_x * np.sin(np.sqrt(2)/2)
        # self.fy = r_y * np.cos(np.sqrt(2)/2)
        # self.ify = r_y * np.sin(np.sqrt(2)/2)
        # self.fz = r_z * np.cos(phi_z)
        # self.ifz = r_z * np.sin(phi_z)

        v = [self.fx[:], self.ifx[:]]

        print(v)

        theta = np.radians(45)

        r = np.array(((np.cos(theta), -np.sin(theta)),
                      (np.sin(theta), np.cos(theta))))

        self.fabs = self.absolute_field()

        # phi_x = cmath.phase(self.fx + self.ifx[:] * j)

        # self.fx = self.fx * np.cos(phi)
        # self.ifx = self.ifx * np.sin(phi)

        # self.fy = self.fy * np.cos(phi)
        # self.ify = self.ify * np.sin(phi)
        # self.fz = data[:, 7]
        # self.ifz = data[:, 8]

    def filter(self, alpha):
        self.field[self.field > alpha] = np.nan

    def weighting(self, weight):

        print(np.linalg.norm(self.fabs))
        self.fabs = weight * self.fabs

    def get_absolute_field(self):
        return self.fabs

    def get_field(self):
        return self.field

    def absolute_field(self):
        """
        Compute the absolute field

        Return
        ------
        Absolute field
        """

        return np.sqrt(self.fx ** 2 + self.fy ** 2 + self.fz ** 2 +
                       self.ifx ** 2 + self.ify ** 2 + self.ifz ** 2)

    def normal(self, direction=Normal.DIRECTION_X, position=0):
        """
        Based on the handed over parameter, the view on the resulted field
        will be set.

        Parameters
        ----------
            direction: Normal enum
                Set the normal direction
            position:
                Set the position of the sectional view
        """

        global PLOT_X_LABEL
        global PLOT_Y_LABEL

        if direction == Normal.DIRECTION_X:
            direction = self.x
            vec1_temp = self.y
            vec2_temp = self.z

            PLOT_X_LABEL = 'y/mm'
            PLOT_Y_LABEL = 'z/mm'

        elif direction == Normal.DIRECTION_Y:
            direction = self.y
            vec1_temp = self.z
            vec2_temp = self.x

            PLOT_X_LABEL = 'z/mm'
            PLOT_Y_LABEL = 'x/mm'

        elif direction == Normal.DIRECTION_Z:
            direction = self.z
            vec1_temp = self.x
            vec2_temp = self.y

            PLOT_X_LABEL = 'x/mm'
            PLOT_Y_LABEL = 'y/mm'

        idx = np.abs(direction - position).argmin()
        val = direction.flat[idx]

        vec1 = []
        vec2 = []
        field = []

        for i in range(0, len(direction)):
            if direction[i] != val:
                continue
            else:
                vec1.append(vec1_temp[i])
                vec2.append(vec2_temp[i])
                field.append(self.fabs[i])

        self.vec1, self.vec2 = np.mgrid[np.min(vec1):np.max(vec1):1000j,
                               np.min(vec2):np.max(vec2):1000j]

        self.field = griddata(np.c_[vec1, vec2], np.array(field),
                              (self.vec1, self.vec2), method='linear')

    def show(self):
        """
        Plot the field result
        """

        fig, ax = plt.subplots(figsize=(20, 10))
        plt.xlabel(PLOT_X_LABEL)
        plt.ylabel(PLOT_Y_LABEL)

        cs = plt.imshow(self.field.T, origin='lower', interpolation='gaussian',
                        norm=LogNorm())
        cbar = fig.colorbar(cs, label=FIELD_TYPE)
        plt.show()


def mean_absolute_percentage_error(field1, field2):
    """
    Compute the mean absolute percentage error between
    two resulted fields.
    :param field1: numpy array
    :param field2: numpy array
    :return: percentage error --> float
    """
    error = field1.get_absolute_field() - field2.get_absolute_field()
    error = error / field1.get_absolute_field()
    error = np.abs(error)
    error = np.mean(error)
    return error * 100


def accuracy(field1, field2):
    """
    Compute based on the mean absolute percentage error the
    similarity in percent.
    :param field1: numpy array
    :param field2: numpy array
    :return: float
    """
    accuracy_ = 1 - mean_absolute_percentage_error(field1, field2)
    print("Accuracy: " + "{:.4%}".format(accuracy_))
    return accuracy_
