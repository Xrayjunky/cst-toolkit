from csttoolkit.result import *

if __name__ == "__main__":
    field1 = CSTPlotter('data/result/human/Upper_Leg_E-Field_H_XY.txt')
    field1.normal(Normal.DIRECTION_Z, -0.215)

    field2 = CSTPlotter(['data/result/human/Upper_Leg_E-Field_H_X.txt',
                         'data/result/human/Upper_Leg_E-Field_H_Y.txt'])
    field2.normal(Normal.DIRECTION_Z, -0.215)

    accuracy = accuracy(field1, field2)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    cs1 = ax1.imshow(field1.get_field().T, origin='lower', interpolation='gaussian', norm=LogNorm())
    ax1.set_title("Exact")
    ax1.set_xlabel(PLOT_X_LABEL)
    ax1.set_ylabel(PLOT_Y_LABEL)

    cs2 = ax2.imshow(field2.get_field().T, origin='lower', interpolation='gaussian', norm=LogNorm())
    ax2.set_title("Superposition \n (Accuracy: " + "{:.3%}".format(accuracy) + ")")
    fig.colorbar(cs1, label=FIELD_TYPE, ax=ax1, shrink=0.5)
    fig.colorbar(cs2, label=FIELD_TYPE, ax=ax2, shrink=0.5)
    ax2.set_xlabel(PLOT_X_LABEL)
    ax2.set_ylabel(PLOT_Y_LABEL)
    plt.show()