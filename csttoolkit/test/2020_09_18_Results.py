from csttoolkit.result import *
from matplotlib.backends.backend_pdf import PdfPages

import datetime
import numpy as np
import matplotlib.pyplot as plt


ROOT_FOLDER_RESULT = "C:/Users/Steven/OneDrive/Desktop/Results/"


def plot_field(field_exact, field_one, field_two, field_sup, normal, position, alpha):
    field_exact.normal(normal, position)
    field_one.normal(normal, position)
    field_two.normal(normal, position)
    field_sup.normal(normal, position)

    field_exact.filter(alpha)
    field_sup.filter(alpha)
    field_one.filter(alpha)
    field_two.filter(alpha)

    # Compute diff
    accuracy_ = accuracy(field_exact, field_sup)

    # x direction (Simple)
    fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3, figsize=(20, 14))
    cs1 = ax1.imshow(field_exact.get_field().T, origin='lower', interpolation='gaussian', norm=LogNorm())
    ax1.set_title("Exact")
    fig.colorbar(cs1, label=FIELD_TYPE, ax=ax1, shrink=0.8)
    ax1.set_xlabel(PLOT_X_LABEL)
    ax1.set_ylabel(PLOT_Y_LABEL)

    cs2 = ax2.imshow(field_sup.get_field().T, origin='lower', interpolation='gaussian', norm=LogNorm())
    ax2.set_title("Superposition (Accuracy: " + "{:.2%}".format(accuracy_) + ')')
    fig.colorbar(cs2, label=FIELD_TYPE, ax=ax2, shrink=0.8)
    ax2.set_xlabel(PLOT_X_LABEL)
    ax2.set_ylabel(PLOT_Y_LABEL)

    cs3 = ax3.imshow(
        (abs(field_exact.get_field().T) - abs(field_sup.get_field().T)) / abs(field_exact.get_field()) * 100,
        origin='lower', interpolation='gaussian', norm=LogNorm())
    ax3.set_title("Difference")
    #fig.colorbar(cs3, label=FIELD_TYPE, ax=ax3, shrink=0.8)

    ax3.set_xlabel(PLOT_X_LABEL)
    ax3.set_ylabel(PLOT_Y_LABEL)

    cs4 = ax4.imshow(field_one.get_field().T, origin='lower', interpolation='gaussian', norm=LogNorm())
    ax4.set_title('Field 1')
    fig.colorbar(cs4, label=FIELD_TYPE, ax=ax4, shrink=0.8)
    ax4.set_xlabel(PLOT_X_LABEL)
    ax4.set_ylabel(PLOT_Y_LABEL)

    cs5 = ax5.imshow(field_two.get_field().T, origin='lower', interpolation='gaussian', norm=LogNorm())
    ax5.set_title('Field 2')
    fig.colorbar(cs5, label=FIELD_TYPE, ax=ax5, shrink=0.8)
    ax5.set_xlabel(PLOT_X_LABEL)
    ax5.set_ylabel(PLOT_Y_LABEL)

    cs6 = ax6.imshow(field_sup.get_field().T, origin='lower', interpolation='gaussian', norm=LogNorm())
    ax6.set_title('Superposition')
    fig.colorbar(cs6, label=FIELD_TYPE, ax=ax6, shrink=0.8)
    ax6.set_xlabel(PLOT_X_LABEL)
    ax6.set_ylabel(PLOT_Y_LABEL)

    plt.figure()
    plt.title("Sx")
    fig.text(4.25 / 8.5, 0.5 / 11., str(1), ha='center', fontsize=8)
    pdf.savefig(figure=fig)
    plt.close()

if __name__=="__main__":
    # Save results in a pdf file
    with PdfPages('results.pdf') as pdf:

        # Superposition of two magnetic field with the same propagation

        # Propagation in x direction
        field_exact_ = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sx/E-Field-Hyz.txt")
        field_one_ = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sx/E-Field-Hy.txt")
        field_two_ = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sx/E-Field-Hz.txt")
        field_sup_ = CSTPlotter([ROOT_FOLDER_RESULT + "Simple/Sx/E-Field-Hy.txt",
                                ROOT_FOLDER_RESULT + "Simple/Sx/E-Field-Hz.txt"])

        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_Z, position=-0.192)
        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_X, position=0.125)

        field_exact = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sy/E-Field-Hxz.txt")
        field_one = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sy/E-Field-Hx.txt")
        field_two = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sy/E-Field-Hz.txt")
        field_sup = CSTPlotter([ROOT_FOLDER_RESULT + "Simple/Sy/E-Field-Hx.txt",
                                ROOT_FOLDER_RESULT + "Simple/Sy/E-Field-Hz.txt"])

        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_Z, position=-0.192)
        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_X, position=0.125)

        field_exact_ = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sz/E-Field-Hxy.txt")
        field_one_ = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sz/E-Field-Hx.txt")
        field_two_ = CSTPlotter(ROOT_FOLDER_RESULT + "Simple/Sz/E-Field-Hy.txt")
        field_sup_ = CSTPlotter([ROOT_FOLDER_RESULT + "Simple/Sz/E-Field-Hx.txt",
                                ROOT_FOLDER_RESULT + "Simple/Sz/E-Field-Hy.txt"])

        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_Z, position=-0.192)
        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_X, position=0.125)

        field_exact_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Sxy/E-Field-Hxy.txt")
        field_one_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Sxy/E-Field-Hx.txt")
        field_two_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Sxy/E-Field-Hy.txt")
        field_sup_ = CSTPlotter([ROOT_FOLDER_RESULT + "Mixed/Sxy/E-Field-Hx.txt",
                                ROOT_FOLDER_RESULT + "Mixed/Sxy/E-Field-Hy.txt"])

        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_Z, position=-0.192)
        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_X, position=0.125)

        field_exact_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Sxz/E-Field-Hxz.txt")
        field_one_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Sxz/E-Field-Hx.txt")
        field_two_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Sxz/E-Field-Hz.txt")
        field_sup_ = CSTPlotter([ROOT_FOLDER_RESULT + "Mixed/Sxz/E-Field-Hx.txt",
                                ROOT_FOLDER_RESULT + "Mixed/Sxz/E-Field-Hz.txt"])

        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_Z, position=-0.192)
        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_X, position=0.125)

        field_exact_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Syz/E-Field-Hyz.txt")
        field_one_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Syz/E-Field-Hy.txt")
        field_two_ = CSTPlotter(ROOT_FOLDER_RESULT + "Mixed/Syz/E-Field-Hz.txt")
        field_sup_ = CSTPlotter([ROOT_FOLDER_RESULT + "Mixed/Syz/E-Field-Hy.txt",
                                ROOT_FOLDER_RESULT + "Mixed/Syz/E-Field-Hz.txt"])

        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_Z, position=-0.192)
        plot_field(field_exact=field_exact_, field_one=field_one_, field_two=field_two_, field_sup=field_sup_,
                   alpha=3e-2, normal=Normal.DIRECTION_X, position=0.125)

